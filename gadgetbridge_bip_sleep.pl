#!/usr/bin/perl -F, -al

use POSIX;
use constant HOUR => 2;
use constant LIGHT_SLEEP => 9;
use constant DEEP_SLEEP => 11;

# skip header
$. == 1 and /^\D/ and next;

# timestamp to array
@prev_time = @time;
@time = localtime($F[0]);

# format date
$date = strftime("%Y-%m-%d", @time);

$noon = ($time[HOUR] >= 12 and $prev_time[HOUR] < 12);

# state is in the second to last field, in the lower bits
$state = $F[5] & 0x0f;

# heart rate is in the last field
$hr = 0 + $F[6];

# just copy state, if no real state is set
if ($state == 0 or $state == 10 or $state == -1 or $state == 3) {	# no change; or state not set; or ignore; or not wearing
	$state = $prev_state;
}

# get minimal heart frequency during sleep
if ($hr != 255 and $hr > 0 and ($state == LIGHT_SLEEP or $state == DEEP_SLEEP)) {
	$min_hr ||= INF;
	$min_hr = ($min_hr < $hr ? $min_hr : $hr);
}

# state has changed, or end of day (= noon), or end of file
if ($state != $prev_state or $noon or eof) {

	if ($prev_state == LIGHT_SLEEP) {
		$light_sleep += $F[0] - ($begin_sleep || $F[0]);
	}
	elsif ($prev_state == DEEP_SLEEP) {
		$deep_sleep += $F[0] - ($begin_sleep || $F[0]);
	}

	if ($noon or eof) {

		$total_sleep = $light_sleep + $deep_sleep;

		print join "\t", $date, map { $_ / 3600 } ($total_sleep, $deep_sleep), $min_hr if $total_sleep;	# in hours

		$light_sleep = 0;
		$deep_sleep = 0;
		$min_hr = undef;
	}

	$begin_sleep = $F[0] if ($state == LIGHT_SLEEP or $state == DEEP_SLEEP);
}

$prev_state = $state;
