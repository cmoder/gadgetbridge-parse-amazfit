#!/bin/bash

DATABASE="Gadgetbridge"

# convert SQLite database to CSV, and compute sleep data
echo -e ".headers on\n.mode csv\nSELECT * FROM MI_BAND_ACTIVITY_SAMPLE;" |
	sqlite3 "$DATABASE" |
	./gadgetbridge_bip_sleep.pl
