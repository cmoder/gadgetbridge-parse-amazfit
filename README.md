Gadgetbridge and Amazfit Bip
============================

The [Amazfit Bip watch](https://en.amazfit.com/bip.html) is a simple and very
reliable smartwatch, and it works very well with the open-source app
[Gadgetbridge](https://gadgetbridge.org/).

For me, the key features of the watch are recording sleep and heart rate. So
this is a simple script for calculating the daily amount of sleep from the
Gadgetbridge SQLite database (in a file named *Gadgetbridge*).

